import {html} from "./lib.js"
import {Greet} from "./Greet.js"
import {Sum} from "./Sum.js"
export const App = () =>{
  console.log("app1")
  return (
    html`<div>${Sum({num1 : 1,num2 : 4 }) } 
        ${Greet({msg : "hi hello"})}</div>` 
    )
}