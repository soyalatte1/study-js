//순서 테스트를 위한 모듈
import {html} from "./lib.js"
import {Greet} from "./Greet.js"
import {Sum} from "./Sum.js"
export const App = () =>{
  console.log("app2")
  return (
    html`<div>${Sum({num1 : 4,num2 : 7 }) } 
        ${Greet({msg : "hi hello"})}</div>` 
    )
}