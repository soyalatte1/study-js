import React from "https://cdn.skypack.dev/react"
import htm from "https://unpkg.com/htm?module"

export const html = htm.bind(React.createElement)

export * from "https://cdn.skypack.dev/react"

export * from "https://cdn.skypack.dev/react-dom"