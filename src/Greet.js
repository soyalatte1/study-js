import {html} from "./lib.js"

export const Greet = ({msg = ""}) =>{
  return html `<h2>${msg}</h2>`
}